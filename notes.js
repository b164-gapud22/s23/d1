/*CRUD Operations
	are the heart of any backend
	This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
*/


//Create (Inserting documents)


//Insert One document

/*Syntax:
	db.collectionName.insertOne({ object })
	db.collectionName.insert({ object })
Javascript Syntax comparison:
	object.object.method({ object })

The mongo shell also uses js for syntax which makes it convenient for us to understand it's code. Creating mongoDB syntax in a text editor makes it easy for us to modify code.
*/

//sample:

db.users.insert(
   {
	firstName : Jane,
    lastName : Doe,
    age : 21.0,
    contact : {
        phone : "87654321",
        email : "jane@gmail.com"
    },
    courses : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    department : "none"
}
)


//Insert Many

/*Syntax:
	db.collectionName.insertMany([{ objectA }, {objectB}])
*/

//sample:
db.users.insertMany([
    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 76,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
    
 ])



//Read (Finding Documents)
//Find
/*
Syntax:
	db.collectionName.find();
	db.collectionName.find({ field: value });


*/

//Leaving the search criteria empty will retrieve ALL the documents

db.users.find({});

//Finding single document

db.users.find({
    
    firstName: "Stephen"
    
    })


//Finding with multiple parameters
db.users.find(
{
   lastName: "Armstrong", age: 82
}
    
    
)


//Update Documents
/*
Syntax:
	db.collectionName.updateOne({ criteria }, {$set: {field: value} })
*/
//for update demo: insert this document
// Creating a document to update
		db.users.insert({
		    firstName: "Test",
		    lastName: "Test",
		    age: 0,
		    contact: {
		        phone: "00000000",
		        email: "test@gmail.com"
		    },
		    courses: [],
		    department: "none"
		});

//updateOne()
db.users.updateOne(
		    { firstName: "Test" },
		    {
		        $set : {
		            firstName: "Bill",
		            lastName: "Gates",
		            age: 65,
		            contact: {
		                phone: "12345678",
		                email: "bill@gmail.com"
		            },
		            courses: ["PHP", "Laravel", "HTML"],
		            department: "Operations",
		            status: "active"
		        }
		    }
		);


//Replace One
//can be used if replacing the whole document is necessary
//db.collectionName.replaceOne( {criteria}, {field: value} )

db.users.replaceOne(
    { firstName: "Bill" },
    {
            
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@gmail.com"
                },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations"
            
            }       
  
)


// Updating multiple documents
/*
Syntax
	db.collectionName.updateMany( {criteria}, {$set: {field:value}});
*/
	db.users.updateMany(
		  { department: "none" },
		  {
		    $set: { department: "HR" }
		  }
	);



//Delete Document


// Creating a document to delete for demo
		db.users.insert({
		    firstName: "test",
		    lastName: "test"
		});


//Deleting a single document
//syntax: db.collectionName.deleteOne({ criteria})
db.users.deleteOne({
	firstName: "test"
});



//delete many
//Be careful when using the deleteMany method. If no search criteria is provided, it will delete all documents in a database.
//DO NOT USE: db.collectionName.deleteMany()

//Syntax: db.collectionName.deleteMany({ criteria })



//ADVANCE QUERIES
/*
- Retrieving data with complex data structures is also a good skill for any developer to have.
- Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
- Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/


// Query an embedded document
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
		}
})

// Query on nested field
db.users.find(
	{"contact.email": "janedoe@gmail.com"}
)

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "Javascript", "Python" ] } )

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } )

//Querrying an Embedded Array of objects
//insert data for demo:
db.users.insert({
    nameArray: [{
        nameA: "Juan"
    },
    {
        nameB: "Tamad"
    }

]
    }
)


//then:


db.users.find({
    
    nameArray: {nameA: "Juan"}
    
})












